module codeberg.org/librarian/librarian

go 1.16

require (
	codeberg.org/librarian/stream-proxy-ng v0.0.0-20220821230513-8d7c1f70d094 // indirect
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/aymerick/raymond v2.0.2+incompatible
	github.com/dustin/go-humanize v1.0.0
	github.com/gofiber/fiber/v2 v2.34.1
	github.com/gofiber/template v1.6.28
	github.com/gorilla/feeds v1.1.1
	github.com/hashicorp/go-retryablehttp v0.7.1
	github.com/klauspost/compress v1.15.6 // indirect
	github.com/microcosm-cc/bluemonday v1.0.18
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pelletier/go-toml/v2 v2.0.2 // indirect
	github.com/spf13/viper v1.12.0
	github.com/tidwall/gjson v1.14.1
	github.com/yuin/goldmark v1.4.12
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
